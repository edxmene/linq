using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace consoleapp.model
{
    public class car
    {
        public int id {get; set;}
        public string car_make {get; set;}
        public string car_model {get; set;}
        public int car_year {get; set;}
        public int number_of_doors {get; set;}
        public int hp {get; set;}
    }
}