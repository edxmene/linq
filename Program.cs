﻿//Write a query that returns words at least 5 characters long and make them uppercase.
// List<string> list_strings = new List<string>() {"Computer", "Keyboard", "Mouse", "Usb"};

// list_strings.Where(word => word.Count() > 5)
//             .Select(word => word.ToUpper())
//             .ToList()
//             .ForEach(word => Console.WriteLine(word));


//Write a query that returns words starting with letter 'a' and ending with letter 'm'.
// List<string> list_of_words = new List<string>() {"mum", "amsterdam", "bloom", "annom"};

// list_of_words.Where(word => word.StartsWith('a') && word.EndsWith('m'))
//              .ToList()
//              .ForEach(word => Console.WriteLine(word));


//Write a query that returns top 5 numbers from the list of integers in descending order.
// List<int> list_of_numbers = new List<int>() {78, -9, 0, 23, 54,  21, 7, 86};

// list_of_numbers.OrderByDescending(num => num)
//                .Take(5)
//                .ToList()
//                .ForEach(num => Console.WriteLine(num));
// select * from person order by country_of_birth limit 5;


//Write a query that returns list of numbers and their squares only if square is greater than 20
// List<int> list_of_num = new List<int>() {7, 2, 5, 3, 1, 30};
// list_of_num.Where(num => num*num > 20)
//            .Select(num => num*num)
//            .ToList()
//            .ForEach(num => Console.WriteLine(num));
//select *, price*2 as double_price from car where price*2 > 37000;

//Write a query that replaces 'ea' substring with astersik (*) in given list of words.
// List<string> list_of_strings = new List<string>() {"learn", "current", "deal"};
// list_of_strings.Where(word => word.Contains("ea"))
//                .Select(word => word.Replace("ea","*"))
//                .ToList()
//                .ForEach(word => Console.WriteLine(word));

//select REPLACE (make, 'a', '*') from car;
// List<string> list_of_words = new List<string>() {"cow", "dog", "elephant", "cat", "rat", "squirrel", "snake", "stork"};
// var word = list_of_words.OrderByDescending(word => word)
//              .FirstOrDefault(word => word.Contains("e"));
// Console.WriteLine(word);


//Write a query that shuffles sorted array.
// List<int> order_list = new List<int>() {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
// var rnd = new Random();
// order_list.OrderBy(num => rnd.Next()).ToList().ForEach(num => Console.WriteLine(num));


// var chars = new char[] { ')', '!', '@', '#', '$', '%', '^', '&', '*', '(' };
// var encryptedNumber = "#(@*%)$(&$*#&";
// var decryptedNumber = string.Join("-", encryptedNumber.Select(char_ => Array.IndexOf(chars, char_)));
// Console.WriteLine(decryptedNumber);


//Write a query that returns most frequent character in string. Assume that there is only one such character.
string str = "49fjs492jfJs94KfoedK0iejksKdsj3";
// var mostFrequent = str.GroupBy(c => c).OrderByDescending(c => c.Count()).FirstOrDefault().Key;
var mostFrequentCharacter = str.GroupBy(c => c).OrderByDescending(c => c.Count()).First().Key;
Console.WriteLine(mostFrequentCharacter);